import glob
import sys


''' Functions to find the position of the genus, family, or order 
from a file by looping each position of line given. Returns 
the numerical value of the position.'''
def genusPos(line):
for x in range(len(line)):
if line[x].lower().strip() == "genus":
return x
return 0

def familyPos(line):
for x in range(len(line)):
if line[x].lower().strip() == "family":
return x
return 1

def orderPos(line):
for x in range(len(line)):
if line[x].lower().strip() == "order":
return x
return 2

fileList = glob.glob('*.csv') #opens all the files that contain .csv in the directory. 
file = sys.argv[1] #assigns the first argument in the command line to file.
fp1 = open(file, 'w+') #opens file to make it capable of being writen in.
fp1.write("genus\tfamily\torder\n")
genus = 0
family = 1
order = 2

''' Loops through each file in fileList, opening the file and separating
them by lines, then by tab delimited sections. After finding the position 
the columns of the genus family and order and writen into the open file.'''
for fileName in fileList:
lines = open(fileName, "r+U").readlines()
for i in range(len(lines)):
line = lines[i].replace(",,", ",").split("\t") #replaces ",," with "," and returns a list of all words split at a tab.
line = lines[i].replace(",", "\t").split("\t")
if i == 0:
genus = genusPos(line)
family = familyPos(line)
order = orderPos(line)
if line[genus].lower() != "genus" and len(line[genus]) > 1:
fp1.write(line[genus] + "\t" + line[family] + "\t" + line[order])

fp1.write("\n")

fp1.close()




