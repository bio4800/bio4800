import glob
import sys

def genusPos(line):
	for x in range(len(line)):
		if line[x].lower().strip() == "genus":
			return x
	return 0

def familyPos(line):
	for x in range(len(line)):
		if line[x].lower().strip() == "family":
			return x
	return 1

def orderPos(line):
	for x in range(len(line)):
		if line[x].lower().strip() == "order":
			return x
	return 2

fileList = glob.glob('*.csv')
file = sys.argv[1]
fp1 = open(file, 'w+')
fp1.write("genus\tfamily\torder\n")
genus = 0
family = 1
order = 2

for fileName in fileList:
	lines = open(fileName, "r+U").readlines()
	for i in range(len(lines)):
		line = lines[i].replace(",,", ",").split("\t")
		line = lines[i].replace(",", "\t").split("\t")
		if i == 0:
			genus = genusPos(line)
			family = familyPos(line)
			order = orderPos(line)
		
		if line[genus].lower() != "genus" and len(line[genus]) > 1:
			fp1.write(line[genus] + "\t" + line[family] + "\t" + line[order])

	fp1.write("\n")

fp1.close()





