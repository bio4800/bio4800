#! /usr/bin/env python

import sys #imports sys module
import MySQLdb #imports MySQLdb module

InFileName = sys.argv[1] #sets InFileName to running a file

fp1=open(InFileName, 'r+U') #sets fp1 to reading and writing InFileName for the owner

LineNumber = 0 #sets Linenumber equal to 0

MyConnection = MySQLdb.connect(host = "localhost", user = "root", passwd = "manager", db = "bioproject") #makes connection with MySQLdb
MyCursor = MyConnection.cursor() #renames connection with MySQLdb

lines = fp1.readlines()

for line in lines: #for loop
	if LineNumber > 0: #for loop applies if linenumber is greater than 0 
		line = line.strip('\n') #gets rid of end of line characters
		fields = line.split('\t') #makes tab delimited
		
		genus = fields[0]
		family = fields[1]
		order= fields[2]
		
		SQL = """INSERT INTO final SET
genus='%s'
family_tax='%s',
order_tax='%s' ;
""" % (genus, family, order)
		#print SQL
		MyCursor.execute(SQL)
	LineNumber = LineNumber + 1

MyConnection.commit() 

fp1.close() #closes file
MyCursor.close() #closes cursor
MyConnection.close() #closes connection
